package com.wildcodeschool.spring.bookstore.shoppingcart;

public enum Genre {
    FANTASY, HORROR, CRIME, COMEDY
}
