package com.wildcodeschool.spring.bookstore.shoppingcart;

import com.wildcodeschool.spring.bookstore.entity.Author;

import javax.money.Monetary;
import javax.money.MonetaryAmount;
import javax.money.MonetaryAmountFactory;
import javax.money.MonetaryRounding;
import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {
    private List<Book> books = new ArrayList<>();

    private final double FIVE_PERCENT_DISCOUNT = 0.95;

    private final double SEVEN_PERCENT_DISCOUNT = 0.93;

    private final double TEN_PERCENT_DISCOUNT = 0.9;

    public MonetaryAmount calculatePrice() {
        MonetaryAmount overallPrice = Monetary.getDefaultAmountFactory().setNumber(0).setCurrency("EUR").create();

        for (Book book : books) {
            double discount = 1;

            if (countByAuthor(book.getAuthor()) >= 2) {
                discount = SEVEN_PERCENT_DISCOUNT;
            }

            if (countByBook(book) >= 2) {
                discount = TEN_PERCENT_DISCOUNT;
            }

            overallPrice = overallPrice.add(book.getPrice().multiply(discount));
        }

        if (books.size() >= 3) {
            overallPrice = overallPrice.multiply(FIVE_PERCENT_DISCOUNT);
        }

        return overallPrice.with(Monetary.getDefaultRounding());
    }

    private int countByBook(Book bookToCheck) {
        int count = 0;

        for (Book book : books) {
            if (bookToCheck == book) {
                count++;
            }
        }

        return count;
    }

    private int countByAuthor(Author author) {
        int count = 0;

        for (Book book : books) {
            if (author == book.getAuthor()) {
                count++;
            }
        }

        return count;
    }

    public void addBook(Book book) {
        books.add(book);
    }
}
