package com.wildcodeschool.spring.bookstore.shoppingcart;

import com.wildcodeschool.spring.bookstore.entity.Author;
import org.javamoney.moneta.Money;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import javax.money.MonetaryAmount;

public class Book {

    private CurrencyUnit currencyUnit = Monetary.getCurrency("EUR");

    private MonetaryAmount price;

    private String title;

    private Author author;

    private Genre genre;

    public Book(MonetaryAmount price, String title, Author author) {
        this.price = price;
        this.title = title;
        this.author = author;
    }

    public MonetaryAmount getPrice() {
        return price;
    }

    public String getTitle() {
        return title;
    }

    public Author getAuthor() {
        return author;
    }

    public Genre getGenre() {
        return genre;
    }

}
