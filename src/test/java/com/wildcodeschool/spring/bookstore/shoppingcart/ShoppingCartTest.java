package com.wildcodeschool.spring.bookstore.shoppingcart;

import com.wildcodeschool.spring.bookstore.entity.Author;
import org.junit.jupiter.api.Test;

import javax.money.Monetary;
import javax.money.MonetaryAmount;
import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;

@SuppressWarnings("SpellCheckingInspection")
class ShoppingCartTest {

    ShoppingCart underTest = new ShoppingCart();
    Author author0 = new Author(1L, "J.K.", "Rowling", new ArrayList<>());
    Author author1 = new Author(2L, "Ken", "Follet", new ArrayList<>());
    Author author2 = new Author(3L, "Dan", "Brown", new ArrayList<>());
    Author author3 = new Author(4L, "Stephen", "King", new ArrayList<>());
    Author author4 = new Author(5L, "Fjordar", "Dostojewski", new ArrayList<>());

    MonetaryAmount bookPrice = Monetary.getDefaultAmountFactory().setNumber(10).setCurrency("EUR").create();
    
    @Test
    void shouldReturnZeroAsPrice_whenShoppingCartIsEmpty() {
        MonetaryAmount price = underTest.calculatePrice();

        assertThat(price.getNumber().doubleValue()).isEqualTo(0);
    }

    @Test
    void shouldReturnExactPrice_whenShoppingCartHasOneItem() {
        underTest.addBook(new Book(bookPrice, "Harry Potter und der Stein der Weisen", author0));

        MonetaryAmount price = underTest.calculatePrice();

        assertThat(price.getNumber().doubleValue()).isEqualTo(10);
    }

    @Test
    void shouldReturnExactPrice_whenShoppingCartHasTwoItems() {
        underTest.addBook(new Book(bookPrice, "Harry Potter und der Stein der Weisen", author0));
        underTest.addBook(new Book(bookPrice, "Die Säulen der Erde", author1));

        MonetaryAmount price = underTest.calculatePrice();

        assertThat(price.getNumber().doubleValue()).isEqualTo(20);
    }

    @Test
    void shouldReturnDiscountedPrice_whenShoppingCartHasThreeOrMoreItems() {
        underTest.addBook(new Book(bookPrice, "Harry Potter und der Stein der Weisen", author0));
        underTest.addBook(new Book(bookPrice, "Die Säulen der Erde", author1));
        underTest.addBook(new Book(bookPrice, "Illuminati", author2));

        MonetaryAmount price = underTest.calculatePrice();

        assertThat(price.getNumber().doubleValue()).isEqualTo(28.5);
    }

    @Test
    void shouldReturnRoundedDiscountedPrice_whenShoppingCartHasThreeOrMoreItems() {
        underTest.addBook(new Book(Monetary.getDefaultAmountFactory().setNumber(9.99).setCurrency("EUR").create(), "Harry Potter und der Gefangene von Askaban", author0));
        underTest.addBook(new Book(bookPrice, "Die Säulen der Erde", author1));
        underTest.addBook(new Book(bookPrice, "Illuminati", author2));

        MonetaryAmount price = underTest.calculatePrice();

        assertThat(price.getNumber().doubleValue()).isEqualTo(28.49);
    }

    @Test
    void shouldReturnDiscountedPrice_whenShoppingCartHasTwoOrMoreItemsFromSameAuthor() {
        underTest.addBook(new Book(bookPrice, "Harry Potter und der Stein der Weisen", author0));
        underTest.addBook(new Book(bookPrice, "Harry Potter und die Kammer des Schreckens", author0));

        MonetaryAmount price = underTest.calculatePrice();

        assertThat(price.getNumber().doubleValue()).isEqualTo(18.6);
    }

    @Test
    void shouldReturnDiscountedPrice_whenShoppingCartHasTwoOrMoreItemsFromSameAuthorAndOverallMoreThanThreeItems() {
        underTest.addBook(new Book(bookPrice, "Harry Potter und der Stein der Weisen", author0));
        underTest.addBook(new Book(bookPrice, "Harry Potter und die Kammer des Schreckens", author0));
        underTest.addBook(new Book(bookPrice, "Die Säulen der Erde", author1));

        MonetaryAmount price = underTest.calculatePrice();

        assertThat(price.getNumber().doubleValue()).isEqualTo(27.17);
    }

    @Test
    void shouldReturnDiscountedPrice_whenShoppingCartHasTwoOrMoreEqualItems() {
        Book book = new Book(bookPrice, "Harry Potter und der Stein der Weisen", author0);
        underTest.addBook(book);
        underTest.addBook(book);

        MonetaryAmount price = underTest.calculatePrice();

        assertThat(price.getNumber().doubleValue()).isEqualTo(18.0);
    }

    @Test
    void shouldReturnDiscountedPrice_whenShoppingCartHasMultipleItems() {
        underTest.addBook(new Book(bookPrice, "Harry Potter und der Stein der Weisen", author0));
        underTest.addBook(new Book(bookPrice, "Die Säulen der Erde", author1));
        underTest.addBook(new Book(bookPrice, "Illuminati", author2));

        underTest.addBook(new Book(bookPrice, "Es", author3));
        underTest.addBook(new Book(bookPrice, "Friedhof der Kuscheltiere", author3));

        Book book = new Book(bookPrice, "Der Idiot", author4);
        underTest.addBook(book);
        underTest.addBook(book);

        MonetaryAmount price = underTest.calculatePrice();

        assertThat(price.getNumber().doubleValue()).isEqualTo(63.27);
    }
}